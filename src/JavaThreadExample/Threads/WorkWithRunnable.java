package JavaThreadExample.Threads;
/**
 * To make a class runnable, we can implement java.lang.Runnable interface
 * and provide implementation in public void run() method.
 */
public class WorkWithRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("Doing Processing -START "+Thread.currentThread().getName());
        try{
            Thread.sleep(1000);
            //Get database connection, delete unused data from DB
            doProcessing();
            System.out.println("Doing processing - END "+Thread.currentThread().getName());
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    private void doProcessing(){
        try {
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
