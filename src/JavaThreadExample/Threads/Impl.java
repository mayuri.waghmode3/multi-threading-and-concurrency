package JavaThreadExample.Threads;

import JavaThreadExample.Threads.MyThread;
import JavaThreadExample.Threads.WorkWithRunnable;

/**Thread Benefits:
 *  Java Threads are lightweight compared to processes, it takes less time and resource to create a thread.
 *  Threads share their parent process data and code
 *  Context switching between threads is usually less expensive than between processes.
 *  Thread intercommunication is relatively easy than process communication.
 * Java provides two ways to create a thread programmatically:
 *  1) Implementing the java.lang.Runnable interface.  (more functionality rather than just running as Thread)
 *  2) Extending the java.lang.Thread class.
 *  Implementing Runnable is preferred because java supports implementing multiple interfaces.
 *  If you extend Thread class, you can’t extend any other classes.
 */
public class Impl {
    /**
     *  thread, it’s execution depends on the OS implementation of time slicing
     *  and we can’t control their execution.
     *  set threads priority
     *      but even then it doesn’t guarantee that higher priority thread will be executed first.
     * @param ags
     */
//    public static void main(String args[]) {
//        runnableEg();
//        threadClassEg();
//    }
    /**
     * To use this class as Thread,
     * we need to create a Thread object by passing object of this runnable class
     * and then call start() method to execute the run() method in a separate thread.
     */
    public static void runnableEg(){

        Thread t1 = new Thread(new WorkWithRunnable(), "t1");
        Thread t2 = new Thread(new WorkWithRunnable(), "t2");
        System.out.println("Starting Runnable threads");
        t1.start();
        t2.start();
        System.out.println("Runnable threads has been started");
    }
    /**
     * Then we can create it’s object
     * and call start() method to execute our custom java thread class run method.
     */
    public static void threadClassEg() {
        Thread t3 = new MyThread("t3");
        Thread t4 = new MyThread("t4");
        System.out.println("Starting MyThreads");
        t3.start();
        t4.start();
        System.out.println("MyThreads has been started");
    }
}
