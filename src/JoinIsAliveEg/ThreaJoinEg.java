package JoinIsAliveEg;

/**
 * Java Thread join method can be used to pause the current thread execution
 * until unless the specified thread is dead.
 *  The goal of the program is to make sure main is the last thread to finish
 *  and third thread starts only when first one is dead.
 */
public class ThreaJoinEg {
    public static void joinUsage() throws InterruptedException {
        Thread t1 = new Thread(() ->{
            for(int i=0;i<5;i++){
                System.out.println("Hi "+i);
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){ }
            }
        });
        Thread t2 = new Thread(() ->{
            for(int i=0;i<5;i++){
                System.out.println("Hello "+i);
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){ }
            }
        });

        Thread t3 = new Thread(() ->{
            for(int i=0;i<5;i++){
                System.out.println("Namaste "+i);
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){ }
            }
        });
        t1.start();

        //start second thread after waiting for 2 seconds or if it's dead
        t1.join(2000);
        t2.start();
        System.out.println(t1.isAlive());
        //start third thread only when first thread is dead
        t1.join();
        t3.start();
        System.out.println(t1.isAlive());
        //let all threads finish execution before finishing main thread
        t1.join();
        t2.join();
        t3.join();
        System.out.println("END");
    }
}
