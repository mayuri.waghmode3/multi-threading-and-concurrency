package threadSafety;

public class ProcessingThreadSafely implements Runnable{
    //dummy object variable for synchronization
    private Object mutex= new Object();
    private int count = 0;
    @Override
    public void run() {
        for(int i = 1;i<5;i++){
            processSomething(i);
            //count++;  //<- causes data corruption when multiple threads try to access this resource.
            //count++ seems to be an atomic operation, its NOT and causes data corruption.
            synchronized (mutex) {
                count++;
            }

        }
    }


    private void processSomething(int i){
        // processing some job
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public int getCount(){
        return this.count;
    }
}
