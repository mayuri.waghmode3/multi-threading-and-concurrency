package threadSafety;

import java.util.Arrays;

public class ThreadSafety {
    /**
     * multiple threads created from same Object share object variables
     * and this can lead to data inconsistency when the threads are used to read and update the shared data.
     * @throws InterruptedException
     */
    public static void threadSafetyImpl() throws InterruptedException{
        ProcessingThreadSafely pt = new ProcessingThreadSafely();
        Thread t1 = new Thread(pt, "t1");
        t1.start();
        Thread t2 = new Thread(pt, "t2");
        t2.start();
        //wait for threads to finish processing
        t1.join();
        t2.join();
        System.out.println("Processing count="+pt.getCount());
    }

    /**
     * Here is another example where multiple threads are working on same array of Strings
     * and once processed, appending thread name to the array value.
     * @throws InterruptedException
     */
    public static void threadWithStringArrays() throws InterruptedException {
        String[] arr = {"1","2","3","4","5","6"};
        HashMapProcessor hmp = new HashMapProcessor(arr);
        Thread t1=new Thread(hmp, "t1");
        Thread t2=new Thread(hmp, "t2");
        Thread t3=new Thread(hmp, "t3");
        long start = System.currentTimeMillis();
        //start all the threads
        t1.start();t2.start();t3.start();
        //wait for threads to finish
        t1.join();t2.join();t3.join();
        System.out.println("Time taken= "+(System.currentTimeMillis()-start));
        //check the shared variable value now
        System.out.println(Arrays.asList(hmp.getMap()));
    }
}
