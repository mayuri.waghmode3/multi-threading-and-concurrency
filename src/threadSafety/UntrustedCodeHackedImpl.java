package threadSafety;

public class UntrustedCodeHackedImpl {
    Object mutex = new Object();

    /**
     * Hacker code  ->is trying to lock the myObject instance
     * and once it gets the lock, it’s never releasing it
     * this will cause system to go on deadlock and cause Denial of Service (DoS).
     */
    public void blockedMutexResourceIndefinitely(){
        synchronized (mutex){
            while(true) {
                try {
                    // Indefinitely delay myObject
                    Thread.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *  Hacker code ->  is getting lock on class monitor
     *  and not releasing it, it will cause deadlock and DoS in the system.
     */
    public void blockedClassIndefinetly(){

        synchronized (ProcessingThreadSafely.class) {
            while (true) {
                try {
                    Thread.sleep(Integer.MAX_VALUE); // Indefinitely delay MyObject
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     */
    public void untrustedCode(){
        //untrusted code
        UntrustedCodeHackedImpl myObject = new UntrustedCodeHackedImpl();
        //change the lock/mutex Object reference
        myObject.mutex = new Object();
    }
}
