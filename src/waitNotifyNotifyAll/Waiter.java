package waitNotifyNotifyAll;

/**
 * Waiter class: that will wait for other threads to invoke notify methods to complete it’s processing.
 * The current thread which invokes these methods on any object should have the object monitor
 * else it throws java.lang.IllegalMonitorStateException exception.
 */
public class Waiter implements Runnable {
    private Message msg;

    public Waiter(Message m){
        this.msg=m;
    }
    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        //Notice that Waiter thread is owning monitor on Message object using synchronized block.
        synchronized (msg){
            try{
                System.out.println(name+ " waiting to get notified. Time: "+System.currentTimeMillis());
                msg.wait();
            }catch (InterruptedException e){}
        }
        System.out.println(name+" waiter thread got notified. Time:"+System.currentTimeMillis());
        //process the message now
        System.out.println(name+" processed: "+msg.getMsg());
    }
}
