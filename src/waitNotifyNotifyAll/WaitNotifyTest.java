package waitNotifyNotifyAll;

/**
 * The Object class in java contains three final methods
 * that allows threads to communicate about the lock status of a resource
 * These methods are wait(), notify() and notifyAll().
 */
public class WaitNotifyTest {
    public static void test() {
        Message msg = new Message("Process ME!");
        Waiter waiter1 = new Waiter(msg);
        new Thread(waiter1, "Waiter 1").start();

        Waiter waiter2 = new Waiter(msg);
        new Thread(waiter2,"Waiter 2").start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Notifier notifier = new Notifier(msg);
        new Thread(notifier, "notifier").start();
        System.out.println("All the threads are started");
    }
}
