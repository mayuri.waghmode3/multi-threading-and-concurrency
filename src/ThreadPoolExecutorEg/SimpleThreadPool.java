package ThreadPoolExecutorEg;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleThreadPool {
    public static void impl(){
        //Java thread pool manages the pool of worker threads,
        // it contains a queue that keeps tasks waiting to get executed.
        // We can use ThreadPoolExecutor to create thread pool in Java.
        //java.util.concurrent.Executors provide factory
        // and support methods for java.util.concurrent.Executor interface to create the thread pool in java.
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i=0;i<10;i++){
            //Java thread pool manages the collection of Runnable threads.
            Runnable worker = new WorkerThread(""+i);
            executorService.execute(worker);
        }
        executorService.shutdown();
        while (!executorService.isTerminated()){}
        System.out.println("Finished all threads");
    }
}
