package ThreadPoolExecutorEg;

import java.util.concurrent.*;

public class WorkerPool {
    public static void impl() throws InterruptedException {
        //RejectedExecutionHandler implementation
        RejectedExecutionHandlerImpl rejectionHandler = new RejectedExecutionHandlerImpl();
        //Get the ThreadFactory implementation to use
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        //creating the ThreadPoolExecutor
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,4,10
                , TimeUnit.SECONDS, new ArrayBlockingQueue<>(2),threadFactory,rejectionHandler);
        //start the monitoring thread
        MyMonitor monitor = new MyMonitor(threadPoolExecutor,3);
        Thread threadMonitor = new Thread(monitor);
        threadMonitor.start();
        //submit work to the thread pool
        for(int i=0;i<10;i++){
            threadPoolExecutor.execute(new WorkerThread("cmd"+i));
        }
        //shutdown pool
        Thread.sleep(30000);
        threadPoolExecutor.shutdown();
        //shut down the monitor thread
        Thread.sleep(5000);
        monitor.shutdown();

    }
}
