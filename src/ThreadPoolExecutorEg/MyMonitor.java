package ThreadPoolExecutorEg;

import java.util.concurrent.ThreadPoolExecutor;

public class MyMonitor implements Runnable {
    private ThreadPoolExecutor threadPoolExecutor;
    private int seconds;
    private boolean run = true;
    public MyMonitor(ThreadPoolExecutor threadPoolExecutor, int delay){
        this.threadPoolExecutor = threadPoolExecutor;
        this.seconds = delay;
    }
    public void shutdown(){
        this.run = false;
    }
    @Override
    public void run() {
        while(run) {
            System.out.println(
                    String.format("[monitor] [%d/%d] " + "Active: %d" +
                                    ", Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s"
                            , this.threadPoolExecutor.getPoolSize(), threadPoolExecutor.getCorePoolSize()
                            , this.threadPoolExecutor.getActiveCount()
                            , this.threadPoolExecutor.getCompletedTaskCount()
                            , this.threadPoolExecutor.getTaskCount()
                            , this.threadPoolExecutor.isShutdown()
                            , this.threadPoolExecutor.isTerminated()));

            try {
                Thread.sleep(seconds * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
