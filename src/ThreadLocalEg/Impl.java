package ThreadLocalEg;

import java.util.Random;

public class Impl {
    public static void caller() throws InterruptedException {
        ThreadLocalExample threadLocalExample = new ThreadLocalExample();
        for(int i=0;i<5;i++){
            Thread thread = new Thread(threadLocalExample,"T"+i);
            Thread.sleep(new Random().nextInt(1000));
            thread.start();
        }
    }
}
