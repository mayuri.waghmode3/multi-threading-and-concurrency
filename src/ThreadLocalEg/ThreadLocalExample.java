package ThreadLocalEg;

import java.text.SimpleDateFormat;
import java.util.Random;

public class ThreadLocalExample implements Runnable{
    private static final ThreadLocal<String> formatter = new ThreadLocal<String>(){
        @Override
        protected String initialValue(){
            return new String("yyyyMMdd HHmm");
        }
    };


    @Override
    public void run() {
        System.out.println("Thread Name : "+ Thread.currentThread().getName()
                +"Default Formatter :" + formatter.get());
        try {
            Thread.sleep(new Random().nextInt(1000));
        }catch (InterruptedException e){}
        formatter.set(formatter.get().substring(0,8)+ new Random().nextInt(1000));
        System.out.println("Thread Name= "+Thread.currentThread().getName()
                +" formatter = "+ formatter.get());
    }

}
