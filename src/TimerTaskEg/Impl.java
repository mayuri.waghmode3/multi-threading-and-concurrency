package TimerTaskEg;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Java Timer class is thread safe
 * and multiple threads can share a single Timer object without need for external synchronization.
 */
public class Impl {
    public static void  caller() {
        TimerTask timerTask = new MyTimerTask();
        //running timer task as daemon thread
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask,0,10*1000);
        System.out.println("TimerTask started");
        //cancel after sometime
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancel();
        System.out.println("TimerTask cancelled");
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
