package ThreadSleep;

/**
 * 1) It always pause the current thread execution.
 * 2) The actual time thread sleeps before waking up
 * and start execution depends on system timers and schedulers.
 * 3) Thread sleep doesn’t lose any monitors or locks current thread has acquired.
 * 4) Any other thread can interrupt the current thread in sleep,
 * in that case InterruptedException is thrown.
 */
public class ThreadSleep {
    public static void  sleepTime() throws InterruptedException{
        long start = System.currentTimeMillis();
        /**
         * The argument value for milliseconds can’t be negative, else it throws IllegalArgumentException.
         */
        Thread.sleep(2000);
        System.out.println("Sleep time in ms : "+(System.currentTimeMillis()-start));
    }
}
