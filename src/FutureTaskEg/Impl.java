package FutureTaskEg;

import java.util.concurrent.*;

public class Impl {
    public static void callMe(){
        MyCallable myCallable2 = new MyCallable(2000L);
        MyCallable myCallable1 = new MyCallable(1000L);
        FutureTask<String> futureTask1 = new FutureTask<String>(myCallable1);
        FutureTask<String> futureTask2 = new FutureTask<String>(myCallable2);
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(myCallable1);
        executorService.submit(myCallable2);

        while (true){
            try{
                if(futureTask1.isDone() && futureTask2.isDone()){
                    System.out.println("Done");
                    //shut down executor service
                    executorService.shutdown();
                    return;
                }
                if(!futureTask1.isDone()){
                    //wait indefinitely for future task to complete
                    System.out.println("FutureTask1 output="+futureTask1.get());
                }
                System.out.println("Waiting for FutureTask2 to complete");
                String s = futureTask2.get(2L, TimeUnit.MILLISECONDS);
                if(s !=null){
                    System.out.println("FutureTask2 output="+s);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }


    }
}
