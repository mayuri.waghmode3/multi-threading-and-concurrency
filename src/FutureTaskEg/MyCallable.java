package FutureTaskEg;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {
    private Long waitTime;
    public MyCallable(Long timeInMilliSec){
        this.waitTime = timeInMilliSec;
    }
    @Override
    public String call() throws Exception {
        Thread.sleep(this.waitTime);
        return Thread.currentThread().getName();
    }
}
